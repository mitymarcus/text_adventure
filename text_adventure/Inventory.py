from text_adventure.Food import Apple, Food
from text_adventure.Item import Item
from text_adventure.Weapon import Rock
import winsound


class Inventory:
    def __init__(self, items):
        """

        :param items: Item, amount
        """
        self.items = items

    def has_item(self, item: type(Item), amount=1):
        for invitem in self.items: # type: Item
            if isinstance(invitem, type(item)) and invitem.amount >= amount:

                return True
        return False

    def take_item(self, item: Item, amount):
        amount_of_item = self.has_item(item)
        if not amount_of_item:
            print("You don't have this item.")
        elif amount_of_item < amount:
            print(f"You have {amount_of_item}, you need {amount}")

    def give_item(self, item: Item):
        if self.has_item(type(item)):
            self.items[self.items.index(item)].amount += item.amount
        elif not self.has_item(item):
            self.items.append(item)
        else:
            print("Lol I don't know what happened, but something did. Here is the list of items:\n" + str(self.items))
        print(f"ITEM ACQUIRED: {item}")
        return self.items
    def has_food(self):
        for invitem in self.items:
            if isinstance(invitem, Food):
                return True
            else:
                return False