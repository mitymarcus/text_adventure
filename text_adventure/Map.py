from text_adventure.Room import Cave1, Cave3, Cave2, Cave4


class Map:
    def __init__(self):
        self.board = [[Cave1(0, 0), Cave3(0, 1)],[Cave2( 1, 0), Cave4(1, 1)]]
    def __str__(self):
        printed_board = ""
        for x in self.board:
            for y in x:
                printed_board += "[" + str(y) + "] "
            printed_board += "\n"
        return printed_board
    def valid_location(self, x, y):
        if x < 0 or x > len(self.board):
            print("You cannot move there")
            return False
        if y < 0 or y > len(self.board[0]):
            print("You cannot move there")
            return False
        return True