from text_adventure.Food import Food, Apple
from text_adventure.Entity import Entity
from text_adventure.Map import Map
from text_adventure.Interaction import Skeleton, Chest, Interaction
from text_adventure.Inventory import Inventory
from prettytable import PrettyTable

from text_adventure.Question import Question
from text_adventure.Room import Cave1, Room
from text_adventure.Weapon import Rock


class Player(Entity):
    def __init__(self, name, inventory: Inventory, money, health, map: Map):
        super().__init__(name)
        self.health = health
        self.inventory = inventory
        self.money = money
        self.x = 0
        self.y = 0
        self.map = map
    def view_backpack(self):
        print("Items in your backpack: \n")
        t = PrettyTable(["Name", "Amount", "Description"])
        [t.add_row((item.name, item.amount, item.description)) for item in self.inventory.items]
        print(t)
        print(f"You currently have {self.money} gems.")
    def get_max_health(self):
        return 100
    def eat(self, food: Food):
        if self.health < self.get_max_health() and self.inventory.has_item(food):
            self.health = self.health + food
    def move(self, direction):
        if direction == "north":
            if self.map.valid_location(self.x, self.y + 1):
                self.y += 1
        if direction == "south":
            if self.map.valid_location(self.x, self.y - 1):
                self.y -= 1
        if direction == "east":
            if self.map.valid_location(self.x + 1, self.y):
                self.x += 1
        if direction == "west":
            if self.map.valid_location(self.x - 1, self.y):
                self.x -= 1
    def view_map(self):
        printed_board = ""
        for index_x, x in enumerate(self.map.board):
            for index_y, y in enumerate(x):
                if index_y == self.y and index_x == self.x:
                    printed_board += "{" + str(y) + "} "
                else:
                    printed_board += "[" + str(y) + "] "
            printed_board += "\n"
        print(printed_board)
    def do(self, interaction: Interaction):
        interaction.action(self)
    def look_around(self):
        self.do(Question(self.current_room().description, self.current_room().interactions).run())

    def current_room(self):
        return self.map.board[self.x][self.y]

    def execute_cmd(self, main_command):
        cmd = main_command.split(" ")
        commands = [
            ["help", "{command}", "Displays this page"],
            ['move', "{north, south, east, west}", "Moves North, South, East, or West"],
            ["backpack", "none", "Take a peek in your backpack"],
            ["look", "none", "Look around in your near vicinity"],
            ["map", "none", "Look at your Map"]
        ]
        if cmd[0].lower() == "help":
            if len(cmd) == 1:
                pt = PrettyTable(["Command", "Arguments", "Description"])
                for command in commands:
                    pt.add_row(command)
                print(pt)
            elif cmd[1] :
                for command in commands:
                    if command[0] == cmd[1]:
                        [print(command_prop) for command_prop in command]
        elif cmd[0].lower() == "move":
            if len(cmd) == 2:
                self.move(cmd[1])

            else:
                print("Unexpected arguement...")
                print("Expected usage:")
                print()
        elif cmd[0] == "backpack":
            self.view_backpack()
        elif cmd[0] == "look":
            self.look_around()
        elif cmd[0] == "map":
            self.view_map()