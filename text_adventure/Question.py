class Question:
    def __init__(self, title, options):
        self.options = options
        self.title = title

    def run(self):
        print(self.title)
        for index, option in enumerate(self.options):
            print(f"{index+1}. {option.title}")
        choice = int(input("What do you do?"))
        return self.options[choice-1]