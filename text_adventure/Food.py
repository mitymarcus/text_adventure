from text_adventure.Item import Item


class Food(Item):
    def __init__(self, name, cost, health, amount=1):
        super().__init__(name, cost, amount)
        self.description = f"Restores {health} health."
        self.health = health
    def __str__(self):
        return self.name + " restores " + str(self.health) + " health"

class Apple(Food):
    def __init__(self, name="Apple", cost=10, health=2, amount=1):
        super().__init__(name, cost, health, amount)