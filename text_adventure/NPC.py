from text_adventure.Entity import Entity


class NPC(Entity):
    def __init__(self, name, money, health, base_damage, inventory):
        super().__init__(name)
        self.inventory = inventory
        self.base_damage = base_damage
        self.health = health
        self.money = money